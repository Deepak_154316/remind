import {Dimensions} from 'react-native';
import React, {Component} from 'react';
import LoginComponent from '../Components/LoginComponent';
import APIService from '../Common_Feature/Utils/APIService';
import {urlEndpoints} from '../Common_Feature/Utils/APIHelper';
import {getAPIRequestBody} from '../Common_Feature/Utils/APIUtility';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class Login extends Component {
  state = {
    email: '', //'jdowns@rainsoft.com',
    password: '', //'rainsoft',
    emailError: '',
    passError: '',
    secureTxtEntry: true,
    emailErrorFlag: false,
    passErrorFlag: false,
    isLoaderVisible: false,
  };

  componentDidMount() {}

  onPasswordToggle = () => {
    this.setState({secureTxtEntry: !this.state.secureTxtEntry});
  };

  onEmailChange = value => {
    console.log('onEmailChange ' + value);
    this.setState({email: value, emailErrorFlag: false});
  };

  onPassChange = value => {
    console.log('onPassChange ' + value);
    this.setState({password: value, passErrorFlag: false});
  };

  onButtonClick = () => {
    const emailRegex = new RegExp(
      '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$',
    );
    if (this.state.email.trim().length <= 0) {
      this.setState({emailError: 'Email is required', emailErrorFlag: true});
    } else if (!emailRegex.test(this.state.email.trim())) {
      this.setState({
        emailError: 'Please enter a valid email',
        emailErrorFlag: true,
      });
    } else if (this.state.password.trim().length <= 0) {
      this.setState({passError: 'Password is required', passErrorFlag: true});
    } else {
      this.setState({isLoaderVisible: true});
      var params = {
        email: this.state.email, //'jdowns@rainsoft.com',
        password: this.state.password, //'rainsoft',
      };

      const getRequestBody = getAPIRequestBody(params);

      APIService(
        urlEndpoints.login,
        {
          // 'Content-Type': 'application/x-www-form-urlencoded',
          // Accept: 'application/json',
        },
        getRequestBody,
      )
        .then(response => {
          console.log('API CALL::', response.data.authentication_token);
          AsyncStorage.setItem(
            'loginToken',
            response.data.authentication_token,
          );
          this.setState({isLoaderVisible: false});

          this.props.navigation.reset({
            index: 0,
            routes: [{name: 'Home'}],
          });

          //this.props.navigation.navigate('Home');
        })
        .catch(error => {
          this.setState({isLoaderVisible: false});
          alert(error.errors)
        });
    }
  };

  render() {
    return (
      <LoginComponent
        isLoaderVisible={this.state.isLoaderVisible}
        emailAddress="Email Address"
        password="Password"
        onSignIn="Sign IN"
        onEmailChange={this.onEmailChange}
        onPassChange={this.onPassChange}
        onPasswordToggle={this.onPasswordToggle}
        onButtonClick={this.onButtonClick}
        secureTextEntry={this.state.secureTxtEntry}
        emailError={this.state.emailError}
        passError={this.state.passError}
        emailErrorFlag={this.state.emailErrorFlag}
        passErrorFlag={this.state.passErrorFlag}
        width={(Dimensions.get('window').width * 90) / 100}
      />
    );
  }
}
