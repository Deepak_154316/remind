import React, {Component} from 'react';
import HomeComponent from '../Components/HomeComponent';
import APIService from '../Common_Feature/Utils/APIService';
import {urlEndpoints} from '../Common_Feature/Utils/APIHelper';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class Home extends Component {
  state = {
    isLoaderVisible: false,
    responseData: [],
  };

  async componentDidMount() {
    this.setState({isLoaderVisible: true});
    var header = {
      'X-Remind-Auth-Token': await AsyncStorage.getItem('loginToken'), //'34a1f3a8-afd4-4c77-8e6b-1b17bbbce677',
    };

    APIService(urlEndpoints.location, header, '')
      .then(response => {
        this.setState({responseData: response.data[0], isLoaderVisible: false});
        console.log('API CALL::', response.data[0]);
      })
      .catch(error => {
        alert("No Data Found")
        this.setState({isLoaderVisible: false});
        console.log('API Call Error', error);
      });
  }

  render() {
    return (
      <HomeComponent
        isLoaderVisible={this.state.isLoaderVisible}
        responseData={this.state.responseData}
      />
    );
  }
}
