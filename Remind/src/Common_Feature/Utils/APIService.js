import { DELETE, GET, GET_PARAMS, PATCH, POST, PUT, requestConfig } from './APIHelper';
import AxiosInstance from './AxiosInstance';

const getResultData = async (apiURLEndpoint, requestType, requestParams, configuration) => {
  console.log('$ APIURLENDPOINT: ', apiURLEndpoint);
  console.log('$ REQUESTTYPE: ', requestType);
  console.log('$ PARAM: ', requestParams);
  console.log('$ CONFIGURATION: ', configuration);
  let response = {};
  try {
    switch (requestType) {
      case GET_PARAMS:
        response = await AxiosInstance().get(apiURLEndpoint, JSON.parse(requestParams));
        break;
      case GET:
        response = await AxiosInstance().get(apiURLEndpoint, configuration);
        break;
      case POST:
        console.log("apiURLEndpoint::", apiURLEndpoint, "requestParams::", requestParams, "configuration::", configuration)
        response = await AxiosInstance().post(apiURLEndpoint, requestParams);
        break;
      case PUT:
        response = await AxiosInstance().put(apiURLEndpoint, requestParams, configuration);
        break;
      case DELETE:
        response = await AxiosInstance().delete(apiURLEndpoint);
        break;
      case PATCH:
        response = await AxiosInstance().patch(apiURLEndpoint, requestParams);
        break;
      default:
        break;
    }
    console.log('Response: ' + JSON.stringify(response.data));
  } catch (error) {
    throw error.response.data;
  }
  return response;
};

const APIService = (urlEndpoints = {}, requestHeader = {}, requestParams = {}) => {
  const apiURLEndpoint = urlEndpoints.endpoint;
  console.log("apiURLEndpoint:::", apiURLEndpoint)
  const apiMethodType = urlEndpoints.methodType;
  const configuration = requestConfig(urlEndpoints.methodType, requestHeader);
  console.log("requestParams:::", requestParams)
  return getResultData(apiURLEndpoint, apiMethodType, requestParams, configuration);
};

export default APIService;
