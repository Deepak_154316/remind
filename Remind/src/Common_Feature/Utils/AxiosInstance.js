import axios from 'axios';
import { defaultHeaders } from './APIHelper';

const AxiosInstance = (headers = defaultHeaders) =>
  axios.create({
    baseURL: "https://remind.rainsoft.com/api/remindapp/v2/",
    timeout: 20000,
    headers,
  });

export default AxiosInstance;
