import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import theme from '../Themes/theme';

const InputComponent = ({
  onTextChange,
  autoCapitalize,
  changeText,
  ImageProp,
  placeholder,
  secureTextEntry = false,
  error,
  onSubmitEditing,
  rel,
  keyboardType,
  passwordIp,
  autoFocus,
  multiline,
  maxLength,
  numberOfLines,
  underlineColorAndroid,
  isVisiblePassword,
  width = '90%',
  height = 48, //hp('4.4%'),
  pressableTitleAction,
  extraStyleLabel,
  extraStyleTextInput,
  extraStyleTextInputFont,
  placeholderTextColor,
  value = '',
  defValue,
  pressableTitle = undefined,
  isUpperCaseForPressableTitle = false,
  title = undefined,
  isUpperCaseForTitle = true,
  style = undefined,
  errorStyles = undefined,
  notEditable = false,
  nonBorderRadius = false,
  borderColor,
  textColor,
  IconLeftSide,
  textAlign = 'left',
  textAlignVertical = 'center',
  textInputContainerWithTitleStyle,
  errorMessage,
}) => {
  const styles = defaultStyle({
    theme,
    width,
    isUpperCaseForPressableTitle,
    isUpperCaseForTitle,
    notEditable,
    nonBorderRadius,
    height,
    borderColor,
    textColor,
  });

  const [userInputText, setUserInputText] = useState();
  useEffect(() => {
    setUserInputText(value);
  }, [value]);

  console.log('width ' + width);
  return (
    <View style={textInputContainerWithTitleStyle ?? {}}>
      {(title || pressableTitle) && (
        <View style={[styles.titleContainerStyle, extraStyleLabel]}>
          {title && (
            <CommonTextView style={styles.titleStyle}>{title}</CommonTextView>
          )}
          {pressableTitle && (
            <CommonPressableText
              textStyle={styles.pressableTitleStyle}
              onPress={pressableTitleAction}>
              {pressableTitle}
            </CommonPressableText>
          )}
        </View>
      )}
      <View
        style={[
          styles.Container,
          extraStyleTextInput,
        ]}>
        {IconLeftSide && <IconLeftSide style={{marginRight: 5 /*hp(0.5)*/}} />}
        <TextInput
          style={[styles.inputStyle, extraStyleTextInputFont]}
          placeholderTextColor={
            placeholderTextColor ?? theme.COMMON_TEXT_INPUT_PLACEHOLDER_COLOR
          }
          autoCorrect={false}
          secureTextEntry={secureTextEntry}
          placeholder={placeholder}
          onChangeText={textValue => {
            setUserInputText(textValue);
            if (onTextChange) onTextChange(textValue);
          }}
          value={userInputText}
          keyboardType={keyboardType}
          maxLength={maxLength}
          editable={!notEditable}
          textAlign={textAlign}
          textAlignVertical={textAlignVertical}
          numberOfLines={numberOfLines}
          multiline={multiline}
          autoCapitalize={autoCapitalize}
        />
        {ImageProp && <ImageProp />}
        {passwordIp && (
          <TouchableOpacity
            style={{paddingRight: 20 /*hp(2)*/}}
            onPress={isVisiblePassword}>
            {secureTextEntry ? (
              <Image
                source={require('../Assets/Images/eye_off.png')}
                width="10"
                height="10"
                style={styles.iconStyle}
              />
            ) : (
              //<IconEyeClosedSVG width="5"/*{wp(5)}*/ height= "5" /*{hp(5)}*/ style={styles.iconStyle} />
              <Image
                source={require('../Assets/Images/eye.png')}
                width="10"
                height="10"
                style={styles.iconStyle}
              />
              //<IconEyeOpenSVG width="5"/*{wp(5)}*/  height="5"/*{hp(5)}*/ style={styles.iconStyle} />
            )}
          </TouchableOpacity>
        )}
      </View>
      {error && (
        <Text
          style={{
            marginLeft:20,
            marginRight:10,
            marginTop: 10, //hp(1),
            fontSize: 13, //hp(1.3),
            color: 'red',
          }}
          numberOfLines={2}>
          {errorMessage}
        </Text>
      )}
    </View>
  );
};

const defaultStyle = ({
  theme,
  height,
  width,
  isUpperCaseForPressableTitle,
  isUpperCaseForTitle,
  notEditable,
  nonBorderRadius,
  borderColor,
  textColor,
}) =>
  StyleSheet.create({
    titleContainerStyle: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginBottom: 15, //hp(1.5),
    },
    titleStyle: {
      // fontFamily: fonts.family.proximaNovaMedium,
      // fontSize: fonts.size.font13,
      textTransform: isUpperCaseForTitle ? 'uppercase' : 'none',
      color: textColor ?? theme.DROPDOWN_BORDER_COLOR,
    },
    pressableTitleStyle: {
      fontSize: 14, //hp(1.4),
      textTransform: isUpperCaseForPressableTitle ? 'uppercase' : 'none',
      color: theme.TEXT_INPUT_TITLE_COLOR,
    },
    Container: {
      borderWidth: 0, //hp(0),
      flexDirection: 'row',
      alignContent: 'space-around',
      alignItems: 'center',
      justifyContent: 'space-between',
      alignSelf: 'center',
      padding: 0, //wp(0),
      borderRadius: nonBorderRadius ? 0 : 3, //hp(3),
      paddingLeft: 2, //hp(2),
      // paddingRight: wp(3.5),
      minHeight: 60, //hp(5.9),
      height: height,
      borderWidth: 1, //hp(0.1),
      width: width,
      borderColor:
        borderColor ?? notEditable
          ? theme.COMMON_TEXT_INPUT_BORDER_DISABLE_COLOR
          : theme.COMMON_TEXT_INPUT_BORDER_COLOR,
      backgroundColor: notEditable
        ? theme.COMMON_TEXT_INPUT_DISABLED_BG_COLOR
        : theme.COMMON_TEXT_INPUT_BG_COLOR,
    },
    errorStyle: {
      borderWidth: 1, //hp(0.1),
      flexDirection: 'row',
      alignContent: 'space-around',
      alignItems: 'center',
      justifyContent: 'space-between',
      alignSelf: 'center',
      padding: 2, //wp(0.2),
      borderRadius: nonBorderRadius ? 0 : 3, //hp(3),
      paddingLeft: 2, //hp(2),
      minHeight: 60, //hp(5.9),
      height: height,
      borderColor: theme.COMMON_TEXT_INPUT_ERROR_BORDER_COLOR,
      backgroundColor: theme.COMMON_TEXT_INPUT_BG_COLOR,
    },
    inputStyle: {
      flex: 1,
      marginLeft: 10, //hp(Platform.OS === 'ios' ? 1 : 0.5),
      textAlign: 'left',
      // fontFamily: fonts.family.proximaNovaSemibold,
      // fontSize: fonts.size.font14,
      color: notEditable
        ? theme.COMMON_TEXT_INPUT_DISABLED_COLOR
        : theme.COMMON_TEXT_INPUT_TEXT_COLOR,
    },
    iconStyle: {
      width: 30, //wp(6),
      height: 30, //hp(4),
      marginLeft: 2, //wp(3),
    },
  });

export default InputComponent;
