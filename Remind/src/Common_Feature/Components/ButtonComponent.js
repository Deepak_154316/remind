import {View, Text, Animated, StyleSheet, Pressable} from 'react-native';
import React from 'react';
import theme from '../Themes/theme';

const ButtonComponent = ({
  btnText,
  onPress,
  width,
  height,
  disableBtn,
  style,
  animatedViewStyle,
  btnTextStyle,
}) => {
  const animated = new Animated.Value(1);
  const styles = defaultStyle(disableBtn, theme, height, animated, width);
  const fadeIn = () => {
    Animated.timing(animated, {
      toValue: 0.4,
      duration: 100,
      useNativeDriver: true,
    }).start();
  };
  const fadeOut = () => {
    Animated.timing(animated, {
      toValue: 1,
      duration: 200,
      useNativeDriver: true,
    }).start();
  };

  return (
    <View style={[styles.container, style]}>
      <Pressable
        testID="buttonComp"
        disabled={disableBtn}
        onPressIn={fadeIn}
        onPressOut={fadeOut}
        onPress={onPress}>
        <Animated.View
          style={[styles.animatedViewContainer, animatedViewStyle]}>
          <Text
            style={[
              styles.buttonTextStyle,
              {
                color: disableBtn
                  ? theme.DISABLE_BUTTON_TEXT_COLOR
                  : theme.BUTTON_TEXT_COLOR,
              },
              btnTextStyle,
            ]}>
            {btnText}
          </Text>
        </Animated.View>
      </Pressable>
    </View>
  );
};
const defaultStyle = (disableBtn, theme, height, animated, width) =>
  StyleSheet.create({
    container: {width: '100%'},
    animatedViewContainer: {
      opacity: animated,
      backgroundColor: disableBtn
        ? theme.DISABLE_BUTTON_COLOR
        : theme.PRIMARY_BUTTON_COLOR,
      height: height, //? hp(height) : hp(5.9),
      width: width ?? '100%',
      alignSelf: 'center',
      borderRadius: 40, //hp(3),
      justifyContent: 'center',
    },
    buttonTextStyle: {
      alignSelf: 'center',
      textAlignVertical: 'center',
      width: '90%',
      height: 40,
      // fontSize: fonts.size.font18,
      // fontFamily: fonts.family.proximaNovaSemibold,
      textTransform: 'uppercase',
      textAlign: 'center',
    },
  });

export default ButtonComponent;
