import React from 'react';
import { ActivityIndicator, Modal, View } from 'react-native';
import { useSelector } from 'react-redux';

const Loader = (props) => {
const { isLoading} = props;

return (
<Modal visible={isLoading} transparent={true}>
    <View style={{justifyContent: 'center', alignItems: 'center',flex: 1,backgroundColor: 'rgba(0, 0, 0, 0.4)',}}>
        <ActivityIndicator size="large" color="rgba(255, 255, 255, 1)" />
    </View>
</Modal>
);

};



export default Loader;