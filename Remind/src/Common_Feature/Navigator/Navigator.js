import { Text, View } from 'react-native'
import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import SplashComponent from '../../Components/SplashComponent';
import LoginComponent from '../../Components/LoginComponent';
import HomeComponent from '../../Components/HomeComponent';
import Login from '../../Containers/Login';
import Home from '../../Containers/Home';

const Stack = createNativeStackNavigator();

export default class Navigator extends Component {
  render() {
    return (
        <NavigationContainer>
        <Stack.Navigator initialRouteName='Splash'>
          <Stack.Screen name="Splash" component={SplashComponent} options={{ headerShown: false}}/>
          <Stack.Screen name="Login" component={Login} options={{ headerShown: false}}/>
          <Stack.Screen name="Home" component={Home}  options={{ headerShown: false}}/>
        </Stack.Navigator>
      </NavigationContainer>
    )
  }
}