import {ImageBackground, Image, Text, View, FlatList} from 'react-native';
import React, {Component} from 'react';
import styles from '../Styles/HomeStyles';
import LoaderComponents from '../../src/Common_Feature/Components/LoaderComponent';

const bgImage = require('../Common_Feature/Assets/Images/bg_blue.jpg');
const image = require('../Common_Feature/Assets/Images/homeLogo.png');
const homeImage = require('../Common_Feature/Assets/Images/house.png');
const arrowIcon = require('../Common_Feature/Assets/Images/iconArrowRight.png');
const onlineStatus = require('../Common_Feature/Assets/Images/status-green.png');
const disconnectedStatus = require('../Common_Feature/Assets/Images/status-red.png');
const notRegister = require('../Common_Feature/Assets/Images/status-yellow.png');
const iconAdd = require('../Common_Feature/Assets/Images/iconAdd.png');

export default HomeComponent = ({...props}) => {
  const renderItem = items => {
    {
      console.log('items:::', items.item.id);
    }
    return (
      <View style={styles.rowStyle}>
        <View style={styles.lableStyle}>
          {items?.item?.model ? (
            <Text style={styles.labelTextStyle}>
              {items?.item?.model +
                '-' +
                items?.item?.unitSizeName +
                '-' +
                items?.item?.resinTypeName}
            </Text>
          ) : (
            <View></View>
          )}
          <Image style={styles.arrowImageStyle} source={arrowIcon} />
        </View>
        <View style={styles.detailsStyles}>
          <View>
            <Image style={styles.addImageStyle} source={iconAdd} />
            {items?.item?.systemStatusName === 'System Disconnected' ? (
              <Image
                style={styles.statusImageStyle}
                source={disconnectedStatus}
              />
            ) : items?.item?.systemStatusName === 'Registering Account' ? (
              <Image style={styles.statusImageStyle} source={notRegister} />
            ) : (
              <Image style={styles.statusImageStyle} source={onlineStatus} />
            )}
          </View>
          <View style={styles.statusViewStyle}>
            <Text style={styles.statusTextStyle}>
              {items.item.systemStatusName}
            </Text>
          </View>
        </View>

        <View style={styles.devider} />
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <ImageBackground
        source={bgImage}
        resizeMode="cover"
        style={styles.bgImage}>
        <Image source={image} resizeMode="contain" style={styles.headerIcon} />

        <View style={styles.imgBox}>
          <View style={styles.container}>
            <Image
              source={homeImage}
              resizeMode="contain"
              style={styles.houseImageStyle}
            />
            <Text style={styles.headerTextStyle}>
              {props?.responseData?.name}
            </Text>

            <View style={styles.underline} />
            <FlatList
              data={props?.responseData?.devices}
              renderItem={renderItem}
              keyExtractor={item => item.id}
            />
          </View>
        </View>
      </ImageBackground>
      <LoaderComponents isLoading={props?.isLoaderVisible}></LoaderComponents>
    </View>
  );
};
