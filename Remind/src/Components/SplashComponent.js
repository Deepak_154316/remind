import {Text, View, Image, ImageBackground} from 'react-native';
import React, {Component} from 'react';
import styles from '../Styles/SplashStyles';

const image = require('../Common_Feature/Assets/Images/remindLogo-blue.png');
const bgImage = require('../Common_Feature/Assets/Images/bg.jpg');

export default class SplashComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: true,
    };
  }

  componentDidMount() {
    var that = this;
    setTimeout(function(){  
        that.hideSplashScreen();
        that.props.navigation.navigate('Login');
    },3000);
  }

  hideSplashScreen() {
    this.setState({isVisible: false});
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={bgImage}
          resizeMode="cover"
          style={styles.bgImage}>
          <Image source={image} style={styles.image} resizeMode="contain" />
        </ImageBackground>
      </View>
    );
  }
}
