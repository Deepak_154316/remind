import {ImageBackground, Image, Text, View, Switch} from 'react-native';
import React, {Component} from 'react';
import styles from '../Styles/LoginStyles';
import InputComponent from '../Common_Feature/Components/InputComponent';
import ButtonComponent from '../Common_Feature/Components/ButtonComponent';
import LoaderComponents from '../../src/Common_Feature/Components/LoaderComponent';

const bgImage = require('../Common_Feature/Assets/Images/bg.jpg');
const image = require('../Common_Feature/Assets/Images/remindLogo-blue.png');

export default function LoginComponent({...props}) {
  // console.log('props ' + props.onEmailChange);
  return (
    <View style={styles.container}>
      <ImageBackground
        source={bgImage}
        resizeMode="cover"
        style={styles.bgImage}>
        <Text style={styles.centerText}>Welcome</Text>
        <Image source={image} resizeMode="contain" style={styles.image} />
        <View style={styles.underline} />
        <View style={styles.container}>
          <InputComponent
            keyboardType={'email-address'}
            returnKeyType="done"
            placeholder={props.emailAddress}
            errorMessage={props?.emailError}
            error={props.emailErrorFlag}
            width={props.width}
            extraStyleTextInput={styles.inputStyle}
            numberOfLines={1}
            onTextChange={props.onEmailChange}
          />
          <InputComponent
            keyboardType={'default'}
            returnKeyType="done"
            placeholder={'Password'}
            errorMessage={props?.passError}
            error={props.passErrorFlag}
            passwordIp={true}
            width={props.width}
            secureTextEntry={props.secureTextEntry}
            extraStyleTextInput={styles.inputStyle}
            numberOfLines={1}
            isVisiblePassword={props.onPasswordToggle}
            onTextChange={props.onPassChange}
          />
          <View style={styles.saveIDStyle}>
            <Text>Save ID:</Text>
            <Switch />
          </View>
          <ButtonComponent
            btnText={'Sign In'}
            autoCapitalize
            onPress={props.onButtonClick}
            btnTextStyle={styles.btnStyle}
          />
          <View style={styles.bottomTextStyle}>
            <Text>Forgot Password</Text>
            <Text numberOfLines={2} style={styles.bottomLableStyle}>
              Create a {'\n'} New Account
            </Text>
            <Text>About Remind App</Text>
          </View>
        </View>
      </ImageBackground>
      <LoaderComponents isLoading={props?.isLoaderVisible}></LoaderComponents>
    </View>
  );
}
