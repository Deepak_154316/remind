import { Dimensions, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    bgImage: {
        flex: 1,
    },
    headerIcon: {
        width: 150,
        height: 100,
        marginTop: 75,
        alignSelf: 'center',
    },
    imgBox: {
        flex:1,
        backgroundColor: 'rgba(0,0,0,0.3)',
        borderTopLeftRadius: 80,
        borderTopRightRadius: 80,
    },
    headerTextStyle:{
        color:"white",
        fontSize: 20,
        fontWeight: "bold",
        alignSelf: 'center',
    },
    houseImageStyle: {
        width: 180,
        height: 120,
        marginBottom:10,
        alignSelf: 'center',
    },
    underline: {
        borderBottomColor: 'white',
        borderBottomWidth: 1,
        marginLeft:10,
        marginRight:10,
        marginTop:50
    },
    labelTextStyle:{
        color:"white",
        fontSize: 15,
        fontWeight: "bold"
    },
    lableStyle: {
        flex: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        marginLeft:10,
        marginRight:10
    },
    arrowImageStyle:{
        height:15,
        width:15,
    },
    statusImageStyle:{
        height:35,
        width:35,
    },
    addImageStyle:{
        height:10,
        width:10,
        marginLeft:30,
        alignSelf:'flex-end'
    },
    rowStyle:{
        flex:1,
        marginTop:25
    },
    devider: {
        borderBottomColor: 'white',
        borderBottomWidth: 0.5,
        marginLeft:10,
        marginRight:10,
        marginTop:10
    },
    detailsStyles: {
        flex: 1,
        flexDirection:'row',
        marginLeft:10,
        marginRight:10
    },
    statusViewStyle:{
        flex:1,
        alignSelf:'center',
        textAlign:'center',
        alignItems:'center',
    },
    statusTextStyle:{
        textAlign:'center',
        alignItems:'center',
        alignSelf:'center',
        color:"white",
        fontSize: 14,
        fontWeight: "bold"
    }
});

export default styles;
