import {Dimensions, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bgImage: {
    flex: 1,
  },
  image: {
    width: (Dimensions.get('window').width * 80) / 100,
    height: 200,
    alignSelf: 'center',
  },
  centerText: {
    marginTop: 20,
    color: 'black',
    fontWeight: '400',
    textAlign: 'center',
    alignItems: 'center',
  },
  underline: {
    borderBottomColor: 'white',
    borderBottomWidth: 2,
  },
  inputStyle: {
    marginTop: 20,
    color: '#F5F5F5',
  },
  switchStyle: {
    flex: 0.2,
    marginEnd: 16,
  },
  textStyle: {
    flex: 0.8,
    marginTop: 30,
    marginStart: (Dimensions.get('window').width * 5) / 100,
  },
  btnStyle: {
    padding: 10,
    marginTop: 20,
    fontSize: 14,
    backgroundColor: '#1A7DAE',
  },
  saveIDStyle: {
    flexDirection: 'row',
    marginTop: 30,
    marginStart: 20,
    marginEnd: 20,
    justifyContent: 'space-between',
  },
  bottomTextStyle: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 30,
    marginStart: 20,
    marginEnd: 20,
    height: 50,
    justifyContent: 'space-evenly',
  },
  bottomLableStyle: {
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20,
  },
});

export default styles;
