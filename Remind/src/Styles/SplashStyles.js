import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
      container: {
        flex: 1,
      },
      bgImage: {
        flex:1,
        justifyContent: "center"
      },
      image: {
        width:250,
        height:100,
        alignSelf:"center"
      }
})

export default styles


// export default StyleSheet.create({
//     container: {
//         flex: 1,
//       },
//       image: {
//         flex: 1,
//         justifyContent: "center"
//       },
//       text: {
//         color: "white",
//         fontSize: 42,
//         lineHeight: 84,
//         fontWeight: "bold",
//         textAlign: "center",
//         backgroundColor: "#000000c0"
//       }
// });